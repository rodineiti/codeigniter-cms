# CODEIGNITER 2 - CMS

Clone the repository

    git clone git@gitlab.com:rodineiti/codeigniter-cms.git

Switch to the repo folder

    cd codeigniter-cms
    
Edit file config/develpment/database.php, and set connection mysql
    
Dump file codeigniter_cms.sql into database

Run server php or your server (Wamp, Mamp, Xamp), and open in the browser localhost
  
    php -S localhost
    

Front

![image](https://user-images.githubusercontent.com/25492122/103930710-f0b0d200-50fd-11eb-976a-6c934ec64d63.png)

Admin

![image](https://user-images.githubusercontent.com/25492122/103930751-fe665780-50fd-11eb-97bc-8313bd5ce9af.png)