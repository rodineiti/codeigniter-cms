<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('page_model');
    }

    public function index()
    {
        $this->data['pages'] = $this->page_model->get_with_parent();
        $this->data['subview'] = 'admin/page/index';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function edit($id = NULL)
    {
        if ($id) {
            $this->data['page'] = $this->page_model->get($id);
            if ($this->data['page']) {
                $this->data['errors'][] = 'Page could not be found';
            }
        } else {
            $this->data['page'] = $this->page_model->get_new();
        }

        $this->data['pages_to_parents'] = $this->page_model->get_no_parents();

        $rules = $this->page_model->rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
            $data = $this->page_model
                ->array_from_post(array('parent_id', 'template', 'title', 'slug', 'body'));
            $this->page_model->save($data, $id);
            redirect('admin/page');
        }

        $this->data['subview'] = 'admin/page/edit';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function order()
    {
        $this->data['sortable'] = TRUE;
        $this->data['subview'] = 'admin/page/order';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function order_ajax()
    {
        if (isset($_POST['sortable'])) {
            $this->page_model->save_order($_POST['sortable']);
        }
        $this->data['pages'] = $this->page_model->get_nested();
        $this->load->view('admin/page/order_ajax', $this->data);
    }

    public function destroy($id = NULL)
    {
        if ($id) {
            $this->page_model->delete($id);
        }
        redirect('admin/page');
    }

    public function _unique_slug($string)
    {
        $id = $this->uri->segment(4);
        $this->db->where('slug', $this->input->post('slug'));
        !$id || $this->db->where('id !=', $id);
        $page = $this->page_model->get();

        if (count($page)) {
            $this->form_validation->set_message('_unique_slug', '%s should be unique');
            return FALSE;
        }

        return TRUE;
    }
}
