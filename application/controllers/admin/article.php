<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('article_model');
    }

    public function index()
    {
        $this->data['articles'] = $this->article_model->get();
        $this->data['subview'] = 'admin/article/index';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function edit($id = NULL)
    {
        if ($id) {
            $this->data['article'] = $this->article_model->get($id);
            if ($this->data['article']) {
                $this->data['errors'][] = 'Article could not be found';
            }
        } else {
            $this->data['article'] = $this->article_model->get_new();
        }

        $rules = $this->article_model->rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == TRUE) {
            $data = $this->article_model->array_from_post(array('pubdate', 'title', 'slug', 'body'));
            $this->article_model->save($data, $id);
            redirect('admin/article');
        }

        $this->data['subview'] = 'admin/article/edit';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function destroy($id = NULL)
    {
        if ($id) {
            $this->article_model->delete($id);
        }
        redirect('admin/article');
    }
}
