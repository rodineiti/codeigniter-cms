<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article extends Frontend_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->data['recent_news'] = $this->article_model->get_recents();
    }

    public function index($id, $slug)
	{
        $this->article_model->set_published();
        $this->data['article'] = $this->article_model->get($id);

        $this->data['article'] || show_404(uri_string());

        $requested_slug = $this->uri->segment(3);
        $set_slug = $this->data['article']->slug;
        if ($requested_slug != $set_slug) {
            redirect('article/' . $this->data['article']->id . '/' . $this->data['article']->slug, 'location', '301');
        }

        add_meta_title($this->data['article']->title);
        $this->data['subview'] = 'article';
        $this->load->view('_main_layout', $this->data);
	}
}