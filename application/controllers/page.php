<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends Frontend_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
	{
	    $this->data['page'] = $this->page_model->get_by(array('slug' => $this->uri->segment(1)), TRUE);
	    $this->data['page'] || show_404(current_url());

	    $method = '_' . $this->data['page']->template;

	    if (method_exists($this, $method)) {
	        $this->$method();
        } else {
	        log_message('error', 'Could not load template' . $method . ' in file ' . __FILE__ . ' at line ' . __LINE__);
	        show_error('Could not load template' . $method);
        }

	    $this->data['subview'] = $this->data['page']->template;
	    $this->load->view('_main_layout', $this->data);
	}

	private function _page()
    {
        $this->data['recent_news'] = $this->article_model->get_recents();
    }

    private function _homepage()
    {
        $this->load->model('article_model');
        $this->db->limit(6);
        $this->data['articles'] = $this->article_model->get();
    }

    private function _news_archives()
    {
        $this->data['recent_news'] = $this->article_model->get_recents();

        $this->article_model->set_published();
        $count = $this->db->count_all_results('articles');

        $perPage = 4;
        if ($count > $perPage) {
            $this->load->library('pagination');
            $config['base_url'] = site_url($this->uri->segment(1) . '/');
            $config['total_rows'] = $count;
            $config['per_page'] = $perPage;
            $config['uri_segment'] = 2;
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(2);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }

        // Fetch articles
        $this->article_model->set_published();
        $this->db->limit($perPage, $offset);
        $this->data['articles'] = $this->article_model->get();
    }
}