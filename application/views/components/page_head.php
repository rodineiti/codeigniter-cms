<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title><?php echo $meta_title; ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="<?php echo site_url('/assets/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo site_url('/assets/css/styles.css'); ?>" rel="stylesheet">

    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="<?php echo site_url('/assets/js/bootstrap.min.js'); ?>"></script>
</head>