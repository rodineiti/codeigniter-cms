<?php

echo get_ol($pages);

function get_ol($data, $child = FALSE)
{
    $html = '';

    if (count($data)) {
        $html .= $child === FALSE ? '<ol class="sortable">' : '<ol>';

        foreach ($data as $item) {
            $html .= '<li id="list_' . $item['id'] . '">';
            $html .= '<div>' . $item['title'] . '</div>';

            if (isset($item['children']) && count($item['children'])) {
                $html .= get_ol($item['children'], TRUE);
            }

            $html .= '</li>' . PHP_EOL;
        }

        $html .= '</ol>' . PHP_EOL;
    }

    return $html;
}
?>

<script>
    $(document).ready(function () {
        $('.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            maxLevels: 2
        });
    });
</script>
