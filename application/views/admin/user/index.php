<section>
    <h2>Users</h2>
    <?php echo anchor('admin/user/edit', '<i class="icon-plus"></i> Add a user') ?>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>E-mail</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($users)): ?>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo anchor('admin/user/edit/' . $user->id, $user->name); ?></td>
                    <td><?php echo anchor('admin/user/edit/' . $user->id, $user->email); ?></td>
                    <td>
                        <?php echo btn_edit('admin/user/edit/' . $user->id)?>
                    </td>
                    <td>
                        <?php echo btn_delete('admin/user/destroy/' . $user->id)?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="3">We could not find any users</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</section>
