<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->data['meta_title'] = config_item('site_name');
        $this->load->model('page_model');
        $this->load->model('article_model');
        $this->data['menu'] = $this->page_model->get_nested();
        $this->data['news_archive_link'] = $this->page_model->get_archive_link();
    }
}