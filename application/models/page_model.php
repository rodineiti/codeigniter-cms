<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_Model extends MY_Model
{
    protected $_table_name = 'pages';
    protected $_order_by = 'parent_id, order';
    public $rules = array(
        'parent_id' => array(
            'field' => 'parent_id',
            'label' => 'Parent',
            'rules' => 'trim|intval'
        ),
        'template' => array(
            'field' => 'template',
            'label' => 'Template',
            'rules' => 'trim|required|xss_clean'
        ),
        'title' => array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required|max_length[100]|xss_clean'
        ),
        'slug' => array(
            'field' => 'slug',
            'label' => 'Slug',
            'rules' => 'trim|required|max_length[100]|url_title|callback__unique_slug|xss_clean'
        ),
        'body' => array(
            'field' => 'body',
            'label' => 'Body',
            'rules' => 'trim|required'
        ),
    );

    public function get_new()
    {
        $page = new stdClass();
        $page->parent_id = 0;
        $page->title = '';
        $page->slug = '';
        $page->body = '';
        $page->template = 'page';
        return $page;
    }

    public function get_no_parents()
    {
        $this->db->select('id, title');
        $this->db->where('parent_id', 0);
        $pages = parent::get();

        $data = array(0 => 'No parent');
        if (count($pages)) {
            foreach ($pages as $page) {
                $data[$page->id] = $page->title;
            }
        }
        return $data;
    }

    public function get_with_parent($id = NULL, $single = FALSE)
    {
        $this->db->select('pages.*, p.slug as parent_slug');
        $this->db->join('pages as p', 'pages.parent_id = p.id', 'left');
        return parent::get($id, $single);
    }

    public function get_archive_link()
    {
        $page = parent::get_by(array('template' => 'news_archives'), TRUE);
        return isset($page->slug) ? $page->slug : '';
    }

    public function get_nested()
    {
        $this->db->order_by($this->_order_by);
        $pages = $this->db->get('pages')->result_array();

        $data = array();
        foreach ($pages as $page) {
            if (!$page['parent_id']) {
                $data[$page['id']] = $page;
            } else {
                $data[$page['parent_id']]['children'][] = $page;
            }
        }
        return $data;
    }

    public function save_order($pages)
    {
        if (count($pages)) {
            foreach ($pages as $order => $page) {
                if ($page['item_id'] != '') {
                    $data = array('parent_id' => (int)$page['parent_id'], 'order' => $order);
                    $this->db->set($data)
                        ->where($this->_primary_key, $page['item_id'])
                        ->update($this->_table_name);
                    //echo '<pre>' . $this->db->last_query() . '</pre>';
                }
            }
        }
    }

    public function delete($id)
    {
        parent::delete($id);

        $this->db->set(array('parent_id' => 0))
            ->where('parent_id', $id)
            ->update($this->_table_name);

        return TRUE;
    }
}